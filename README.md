# ansible-userify
Install userify cron service

# classic installation
- set `userify_env_creds: false`
- provide `userify_api_id` and `userify_api_key` when running role to install particular server group creds

# baked AMI installation
- set `userify_env_creds: true`
- ensure `/etc/default/userify` exists after instance instantiation with contents (pull with environment from s3)
```
export USERIFY_API_ID=<userify api id>
export USERIFY_API_KEY=<userify api key>
```
